package domerror

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/adforhome/backend/letsgo/tools"
)

func Wrap(src *DomainError, dest error) *DomainError {
	src.Wrap(dest)
	return src
}

func New(key Key, message string, severity Severity) *DomainError {
	return &DomainError{
		Key:      key,
		Message:  message,
		Severity: severity,
	}
}

func SetMessage(err *DomainError, msg string) *DomainError {
	err.Message = msg
	return err
}

// IsOneOf checks if it's a DomainError and if one of targets has same Key
func IsOneOf(err error, targets ...error) bool {
	var ret *DomainError
	if errors.As(err, &ret) && ret.IsOneOf(targets...) {
		return true
	}
	return false
}

func PrintDomainError(err error) {
	fmt.Println(BuildStrError(err, 0))
}

func PrintJSONDomainError(err error, inline bool) {
	if inline {
		fmt.Print(BuildJSONError(err, 0, true))
	} else {
		fmt.Println(BuildJSONError(err, 0, true))
	}
}

func BuildJSONError(err error, deep int, inline bool) string {
	var strB tools.StringBuilder

	//nil case
	if err == nil {
		strB.WriteStr("null")
		return strB.String()
	}

	domainError, ok := err.(DomainError)

	//not domain error cases
	if !ok {
		strB.WriteStringf(`"%s"`, strings.Replace(err.Error(), `"`, "'", -1))
		return strB.String()
	}

	//domain error cases
	if inline {
		strB.WriteStr(`{`)
		strB.WriteStringf(`"Key": "%s",`, domainError.Key)
		strB.WriteStringf(`"Message": "%s",`, strings.Replace(domainError.Message, `"`, "'", -1))
		strB.WriteStringf(`"Severity": "%s",`, domainError.Severity)
		strB.WriteStringf(`"Wrapped": %s`, BuildJSONError(domainError.Unwrap(), 0, true))
		strB.WriteStr(`}`)
	} else {
		strB.WriteStr(`{`).WriteLn()
		strB.WriteLt(deep+1).WriteStringf(`"Key": "%s",`, domainError.Key).WriteLn()
		strB.WriteLt(deep+1).WriteStringf(`"Message": "%s",`, strings.Replace(domainError.Message, `"`, "'", -1)).WriteLn()
		strB.WriteLt(deep+1).WriteStringf(`"Severity": "%s",`, domainError.Severity).WriteLn()
		strB.WriteLt(deep+1).WriteStringf(`"Wrapped": %s`, BuildJSONError(domainError.Unwrap(), deep+2, false)).WriteLn()
		strB.WriteLt(deep).WriteStr(`}`).WriteLn()
	}

	return strB.String()
}

func BuildStrError(err error, deep int) string {
	var strB tools.StringBuilder

	//nil case
	if err == nil {
		strB.WriteStr("<nil>")
		return strB.String()
	}

	domainError, ok := err.(*DomainError)

	//not domain error cases
	if !ok {
		strB.WriteStringf("\"%s\"", err.Error())
		return strB.String()
	}

	//domain error cases
	strB.WriteStr("{").WriteLn()
	strB.WriteLt(deep+1).WriteStringf("Key: \"%s\"", domainError.Key).WriteLn()
	strB.WriteLt(deep+1).WriteStringf("Message: \"%s\"", domainError.Message).WriteLn()
	strB.WriteLt(deep+1).WriteStringf("Severity: \"%s\"", domainError.Severity).WriteLn()
	strB.WriteLt(deep+1).WriteStringf("Wrapped: %s", BuildStrError(domainError.Unwrap(), deep+2)).WriteLn()
	strB.WriteLt(deep).WriteStr("}").WriteLn()

	return strB.String()
}
