package domerror

import "reflect"

const (
	DomainErrorKey      Key = "domain_error"
	UnableMarshalKey    Key = "marshal_error"
	FailedUnmarshalKey  Key = "failed_unmarshal"
	FailedJsonDecodeKey Key = "json_failed_decode"
)

var (
	NotDomainError        = New(DomainErrorKey, "", Warning)
	UnableMarshalError    = New(UnableMarshalKey, "", Error)
	FailedUnmarshalError  = New(FailedUnmarshalKey, "", Error)
	FailedJsonDecodeError = New(FailedJsonDecodeKey, "", Error)
)

func NewNotDomainError(err error) *DomainError {
	return SetMessage(NotDomainError, err.Error())
}

func NewUnableMarshalError(i interface{}, err error) *DomainError {
	domErr := Wrap(UnableMarshalError, err)
	return SetMessage(domErr, "unable to marshal object: "+reflect.TypeOf(i).Name())
}

func NewFailedUnmarshalError(i interface{}, err error) *DomainError {
	domErr := Wrap(FailedUnmarshalError, err)
	return SetMessage(domErr, "failed to unmarshal object: "+reflect.TypeOf(i).Name())
}

func NewFailedJsonDecodeError(i interface{}, err error) *DomainError {
	domErr := Wrap(FailedJsonDecodeError, err)
	return SetMessage(domErr, "failed to decode object: "+reflect.TypeOf(i).Name())
}
