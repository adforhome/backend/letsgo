package domerror

import (
	"encoding/json"
	"errors"
	"strings"
)

type Severity string
type Key string

type DomainError struct {
	Key      Key      `json:"key"`
	Message  string   `json:"message"`
	Severity Severity `json:"severity"`

	Wrapped      error  `json:"-"`
	WrappedError string `json:"wrapped"`
}

const (
	Error   Severity = "ERROR"
	Warning Severity = "WARNING"
	Info    Severity = "INFO"
	Debug   Severity = "DEBUG"
)

func (err DomainError) Error() string {
	return BuildJSONError(err, 0, true)
}

// Unwrap returns the Wrapped error if any or nil.
func (err DomainError) Unwrap() error {
	return err.Wrapped
}

// Wrap sets src as Wrapped error for the DomainError.
func (dest *DomainError) Wrap(src error) {
	dest.Wrapped = src
	dest.WrappedError = src.Error()
}

func (err DomainError) ToJSON() interface{} {
	var inter interface{}
	json.Unmarshal([]byte(strings.ReplaceAll(err.Error(), "\n", " ")), &inter)
	return inter
}

// Is checks whether the target is a DomainError or not.
func (err DomainError) Is(target error) bool {
	var t DomainError

	if errors.As(target, &t) {
		if res := err.Key == t.Key; res {
			return true
		}
	}

	if err.Wrapped != nil {
		return errors.Is(target, err.Wrapped)
	}
	return false
}

// IsOneOf checks whether one of targets has same Status
func (err DomainError) IsOneOf(targets ...error) bool {
	for _, target := range targets {
		if err.Is(target) {
			return true
		}
	}
	return false
}

// As checks if target's a DomainError and sets target to err and returns true.
// If err is Wrapped then finds in the first error in err's chain that matches target, and if so, sets target to that error value and returns true.
func (err DomainError) As(target interface{}) bool {
	if v, ok := target.(*DomainError); ok {
		v.Key = err.Key
		v.Message = err.Message
		v.Severity = err.Severity
		v.Wrapped = err.Wrapped
		return true
	}
	if err.Wrapped != nil {
		return errors.As(err.Wrapped, &target)
	}
	return false
}
