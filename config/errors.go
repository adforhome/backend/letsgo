package config

import "gitlab.com/adforhome/backend/letsgo/domerror"

const (
	MissingConfigKey     domerror.Key = "missing_config"
	UnauthorizedValuekey domerror.Key = "config_unauthorized_value"
)

var (
	MissingConfigError     = domerror.New(MissingConfigKey, "", domerror.Error)
	UnauthorizedValueError = domerror.New(UnauthorizedValuekey, "", domerror.Error)
)

func NewMissingConfigError(config string) *domerror.DomainError {
	return domerror.SetMessage(MissingConfigError, config+" is missing")
}

func NewUnauthorizedValueError(value, valType string) *domerror.DomainError {
	return domerror.SetMessage(UnauthorizedValueError, "unauthorized value want type: "+valType+", got: "+value)
}
