package config

import (
	"strconv"

	"github.com/spf13/viper"

	"gitlab.com/adforhome/backend/letsgo/domerror"
)

func MustConfig(key string) (string, *domerror.DomainError) {
	value := viper.GetString(key)
	if value == "" {
		return "", NewMissingConfigError(key)
	}
	return value, nil
}

func MustConfigInt(key string) (int, *domerror.DomainError) {
	value := viper.GetString(key)
	if value == "" {
		return 0, NewMissingConfigError(key)
	}
	val, err := strconv.Atoi(value)
	if err != nil {
		return 0, NewUnauthorizedValueError(value, "int")
	}

	return val, nil
}
