package validator

import "github.com/go-playground/validator/v10"

type CustomValidator struct {
	Tag string
	Fn  func(fl validator.FieldLevel) bool
}
