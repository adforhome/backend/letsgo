package validator

import (
	"reflect"

	"gitlab.com/adforhome/backend/letsgo/domerror"
)

const (
	ValidationFailedKey         domerror.Key = "validation_failed"
	FailedRegisterValidationKey domerror.Key = "failed_register_validation"
)

var (
	ValidationFailedError         = domerror.New(ValidationFailedKey, "object validation failed", domerror.Error)
	FailedRegisterValidationError = domerror.New(FailedRegisterValidationKey, "failed to register new validator", domerror.Error)
)

func NewValidationError(i interface{}, err error) *domerror.DomainError {
	domErr := domerror.SetMessage(ValidationFailedError, "Invalid value object: "+reflect.TypeOf(i).Name())
	domErr.Wrap(err)
	return domErr
}

func NewFailedRegisterValidationError(err error) *domerror.DomainError {
	return domerror.Wrap(FailedRegisterValidationError, err)
}
