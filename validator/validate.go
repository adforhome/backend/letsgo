package validator

import (
	"github.com/go-playground/validator/v10"

	"gitlab.com/adforhome/backend/letsgo/domerror"
)

var validate *validator.Validate

func init() {
	validate = validator.New()
}

func Validate(toValidate interface{}) *domerror.DomainError {
	err := validate.Struct(toValidate)
	if err != nil {
		return NewValidationError(toValidate, err)
	}
	return nil
}

func RegisterValidation(customValidators ...CustomValidator) *domerror.DomainError {
	for _, custom := range customValidators {
		err := validate.RegisterValidation(custom.Tag, custom.Fn)
		if err != nil {
			return NewFailedRegisterValidationError(err)
		}
	}
	return nil
}
