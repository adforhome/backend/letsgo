package handle_retry

import (
	"gitlab.com/adforhome/backend/letsgo/domerror"
)

const (
	MaxRetryKey domerror.Key = "max_retry"
)

var (
	MaxRetryError = domerror.New(MaxRetryKey, "max retry attempted", domerror.Error)
)

func NewMaxRetryError(err error) *domerror.DomainError {
	return domerror.Wrap(MaxRetryError, err)
}
