package handle_retry

import (
	"time"

	"gitlab.com/adforhome/backend/letsgo/domerror"
	"gitlab.com/adforhome/backend/letsgo/logger"
)

type SetHandleRetryOptions func(opts *HandleRetryOptions)

type HandleRetryOptions struct {
	MaxRetry  int
	IgnoreErr []error
}

func HandleRetry(handle func() *domerror.DomainError, opts ...SetHandleRetryOptions) *domerror.DomainError {
	var err *domerror.DomainError

	options := GetHandleRetryOptions(opts...)

	for i := 0; i < options.MaxRetry; i++ {
		err = handle()
		if err == nil || domerror.IsOneOf(err, options.IgnoreErr...) {
			return err
		}
		logger.WithDomainError(nil, err).Warningf("failed to send event retry: %d/%d", i+1, options.MaxRetry)
		// 0,5s | 2s | 4,5s | 8s | 12,5s  => 27,5s
		if i+1 < options.MaxRetry {
			time.Sleep(time.Duration((i+1)*(i+1)*500) * time.Millisecond)
		}
	}
	return NewMaxRetryError(err)
}

// GetHandleRetryOptions return the options build with the received opts
func GetHandleRetryOptions(opts ...SetHandleRetryOptions) HandleRetryOptions {
	var options HandleRetryOptions

	options.MaxRetry = 5

	for _, opt := range opts {
		opt(&options)
	}
	return options
}

func WithMaxRetry(maxRetry int) SetHandleRetryOptions {
	return func(opts *HandleRetryOptions) {
		opts.MaxRetry = maxRetry
	}
}

func IgnoreErrors(errors ...error) SetHandleRetryOptions {
	return func(opts *HandleRetryOptions) {
		opts.IgnoreErr = errors
	}
}
