# github.com/aws/aws-sdk-go v1.34.28
github.com/aws/aws-sdk-go/aws
github.com/aws/aws-sdk-go/aws/awserr
github.com/aws/aws-sdk-go/aws/awsutil
github.com/aws/aws-sdk-go/aws/client/metadata
github.com/aws/aws-sdk-go/aws/credentials
github.com/aws/aws-sdk-go/aws/endpoints
github.com/aws/aws-sdk-go/aws/request
github.com/aws/aws-sdk-go/aws/signer/v4
github.com/aws/aws-sdk-go/internal/context
github.com/aws/aws-sdk-go/internal/ini
github.com/aws/aws-sdk-go/internal/sdkio
github.com/aws/aws-sdk-go/internal/sdkmath
github.com/aws/aws-sdk-go/internal/shareddefaults
github.com/aws/aws-sdk-go/internal/strings
github.com/aws/aws-sdk-go/internal/sync/singleflight
github.com/aws/aws-sdk-go/private/protocol
github.com/aws/aws-sdk-go/private/protocol/rest
# github.com/beorn7/perks v1.0.1
github.com/beorn7/perks/quantile
# github.com/bsm/redis-lock v8.0.0+incompatible
## explicit
github.com/bsm/redis-lock
# github.com/cespare/xxhash/v2 v2.1.1
github.com/cespare/xxhash/v2
# github.com/fsnotify/fsnotify v1.4.7
github.com/fsnotify/fsnotify
# github.com/go-playground/locales v0.13.0
github.com/go-playground/locales
github.com/go-playground/locales/currency
# github.com/go-playground/universal-translator v0.17.0
github.com/go-playground/universal-translator
# github.com/go-playground/validator/v10 v10.4.1
## explicit
github.com/go-playground/validator/v10
# github.com/go-redis/redis v6.15.9+incompatible
## explicit
github.com/go-redis/redis
github.com/go-redis/redis/internal
github.com/go-redis/redis/internal/consistenthash
github.com/go-redis/redis/internal/hashtag
github.com/go-redis/redis/internal/pool
github.com/go-redis/redis/internal/proto
github.com/go-redis/redis/internal/util
# github.com/go-stack/stack v1.8.0
github.com/go-stack/stack
# github.com/golang/protobuf v1.4.3
github.com/golang/protobuf/proto
github.com/golang/protobuf/ptypes
github.com/golang/protobuf/ptypes/any
github.com/golang/protobuf/ptypes/duration
github.com/golang/protobuf/ptypes/timestamp
# github.com/golang/snappy v0.0.1
github.com/golang/snappy
# github.com/google/uuid v1.2.0
## explicit
github.com/google/uuid
# github.com/gorilla/mux v1.8.0
## explicit
github.com/gorilla/mux
# github.com/hashicorp/hcl v1.0.0
github.com/hashicorp/hcl
github.com/hashicorp/hcl/hcl/ast
github.com/hashicorp/hcl/hcl/parser
github.com/hashicorp/hcl/hcl/printer
github.com/hashicorp/hcl/hcl/scanner
github.com/hashicorp/hcl/hcl/strconv
github.com/hashicorp/hcl/hcl/token
github.com/hashicorp/hcl/json/parser
github.com/hashicorp/hcl/json/scanner
github.com/hashicorp/hcl/json/token
# github.com/jmespath/go-jmespath v0.4.0
github.com/jmespath/go-jmespath
# github.com/klauspost/compress v1.9.5
github.com/klauspost/compress/fse
github.com/klauspost/compress/huff0
github.com/klauspost/compress/snappy
github.com/klauspost/compress/zstd
github.com/klauspost/compress/zstd/internal/xxhash
# github.com/leodido/go-urn v1.2.0
github.com/leodido/go-urn
# github.com/magiconair/properties v1.8.1
github.com/magiconair/properties
# github.com/matttproud/golang_protobuf_extensions v1.0.1
github.com/matttproud/golang_protobuf_extensions/pbutil
# github.com/mitchellh/mapstructure v1.1.2
github.com/mitchellh/mapstructure
# github.com/opentracing/opentracing-go v1.2.0
## explicit
# github.com/pelletier/go-toml v1.7.0
github.com/pelletier/go-toml
# github.com/pkg/errors v0.9.1
github.com/pkg/errors
# github.com/prometheus/client_golang v1.10.0
## explicit
github.com/prometheus/client_golang/prometheus
github.com/prometheus/client_golang/prometheus/internal
github.com/prometheus/client_golang/prometheus/promauto
# github.com/prometheus/client_model v0.2.0
github.com/prometheus/client_model/go
# github.com/prometheus/common v0.18.0
github.com/prometheus/common/expfmt
github.com/prometheus/common/internal/bitbucket.org/ww/goautoneg
github.com/prometheus/common/model
# github.com/prometheus/procfs v0.6.0
github.com/prometheus/procfs
github.com/prometheus/procfs/internal/fs
github.com/prometheus/procfs/internal/util
# github.com/sirupsen/logrus v1.8.1
## explicit
github.com/sirupsen/logrus
# github.com/spf13/afero v1.1.2
github.com/spf13/afero
github.com/spf13/afero/mem
# github.com/spf13/cast v1.3.0
github.com/spf13/cast
# github.com/spf13/jwalterweatherman v1.0.0
github.com/spf13/jwalterweatherman
# github.com/spf13/pflag v1.0.3
github.com/spf13/pflag
# github.com/spf13/viper v1.7.1
## explicit
github.com/spf13/viper
# github.com/streadway/amqp v1.0.0
## explicit
github.com/streadway/amqp
# github.com/subosito/gotenv v1.2.0
github.com/subosito/gotenv
# github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c
github.com/xdg/scram
# github.com/xdg/stringprep v0.0.0-20180714160509-73f8eece6fdc
github.com/xdg/stringprep
# go.mongodb.org/mongo-driver v1.4.5
## explicit
go.mongodb.org/mongo-driver/bson
go.mongodb.org/mongo-driver/bson/bsoncodec
go.mongodb.org/mongo-driver/bson/bsonoptions
go.mongodb.org/mongo-driver/bson/bsonrw
go.mongodb.org/mongo-driver/bson/bsontype
go.mongodb.org/mongo-driver/bson/primitive
go.mongodb.org/mongo-driver/event
go.mongodb.org/mongo-driver/internal
go.mongodb.org/mongo-driver/mongo
go.mongodb.org/mongo-driver/mongo/options
go.mongodb.org/mongo-driver/mongo/readconcern
go.mongodb.org/mongo-driver/mongo/readpref
go.mongodb.org/mongo-driver/mongo/writeconcern
go.mongodb.org/mongo-driver/tag
go.mongodb.org/mongo-driver/version
go.mongodb.org/mongo-driver/x/bsonx
go.mongodb.org/mongo-driver/x/bsonx/bsoncore
go.mongodb.org/mongo-driver/x/mongo/driver
go.mongodb.org/mongo-driver/x/mongo/driver/address
go.mongodb.org/mongo-driver/x/mongo/driver/auth
go.mongodb.org/mongo-driver/x/mongo/driver/auth/internal/gssapi
go.mongodb.org/mongo-driver/x/mongo/driver/connstring
go.mongodb.org/mongo-driver/x/mongo/driver/description
go.mongodb.org/mongo-driver/x/mongo/driver/dns
go.mongodb.org/mongo-driver/x/mongo/driver/mongocrypt
go.mongodb.org/mongo-driver/x/mongo/driver/mongocrypt/options
go.mongodb.org/mongo-driver/x/mongo/driver/ocsp
go.mongodb.org/mongo-driver/x/mongo/driver/operation
go.mongodb.org/mongo-driver/x/mongo/driver/session
go.mongodb.org/mongo-driver/x/mongo/driver/topology
go.mongodb.org/mongo-driver/x/mongo/driver/uuid
go.mongodb.org/mongo-driver/x/mongo/driver/wiremessage
# golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
golang.org/x/crypto/ocsp
golang.org/x/crypto/pbkdf2
golang.org/x/crypto/sha3
# golang.org/x/net v0.0.0-20201021035429-f5854403a974
## explicit
golang.org/x/net/context/ctxhttp
# golang.org/x/sync v0.0.0-20201207232520-09787c993a3a
golang.org/x/sync/errgroup
golang.org/x/sync/semaphore
# golang.org/x/sys v0.0.0-20210309074719-68d13333faf2
golang.org/x/sys/cpu
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.3.3
golang.org/x/text/transform
golang.org/x/text/unicode/norm
# google.golang.org/protobuf v1.23.0
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/fieldnum
google.golang.org/protobuf/internal/fieldsort
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genname
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/mapsort
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/known/anypb
google.golang.org/protobuf/types/known/durationpb
google.golang.org/protobuf/types/known/timestamppb
# gopkg.in/ini.v1 v1.51.0
gopkg.in/ini.v1
# gopkg.in/yaml.v2 v2.3.0
gopkg.in/yaml.v2
