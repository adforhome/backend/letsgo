package event

import (
	"context"

	"gitlab.com/adforhome/backend/letsgo/domerror"
	rabbitmq "gitlab.com/adforhome/backend/letsgo/rabbit"
)

type FailedEvent struct {
	Error          *domerror.DomainError `json:"error"`
	RequestPayload interface{}           `json:"request_payload"`
	RoutingKey     string                `json:"-"`
	Exchange       string                `json:"-"`
}

func NewFailedEvent(routingKey, exchangeName string, requestPayload interface{}, err *domerror.DomainError) FailedEvent {
	return FailedEvent{
		Error:          err,
		RequestPayload: requestPayload,
		RoutingKey:     routingKey,
		Exchange:       exchangeName,
	}
}

func (e FailedEvent) Send(ctx context.Context, client *rabbitmq.Client) *domerror.DomainError {
	return client.EmitHeaderBodyEvent(ctx, rabbitmq.EventEmitter{
		Exchange:   e.Exchange,
		RoutingKey: e.RoutingKey,
		Body:       e,
	})
}
