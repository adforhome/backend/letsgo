package rabbitmq

import (
	"gitlab.com/adforhome/backend/letsgo/domerror"
)

const (
	UnableToConnectKey        domerror.Key = "rabbit_unable_to_connect"
	FailedOpenChannelKey      domerror.Key = "rabbit_failed_open_channel"
	UnableDeclareExchangeKey  domerror.Key = "rabbit_unable_to_declare_exchange"
	UnableDeclareQueueKey     domerror.Key = "rabbit_unable_to_declare_queue"
	UnableDeleteQueueKey      domerror.Key = "rabbit_unable_to_delete_queue"
	UnableBindQueueKey        domerror.Key = "rabbit_unable_to_bind_queue"
	UnableUnbindQueueKey      domerror.Key = "rabbit_unable_to_unbind_queue"
	UnablePublishMessageKey   domerror.Key = "rabbit_unable_publish_message"
	UnableQoSKey              domerror.Key = "rabbit_unable_qos"
	UnableAckMessageKey       domerror.Key = "rabbit_unable_acknowledge_message"
	FailedRegisterConsumerKey domerror.Key = "rabbit_failed_register_consumer"
)

var (
	UnableToConnectError        = domerror.New(UnableToConnectKey, "failed to connect to RabbitMQ", domerror.Error)
	FailedOpenChannelError      = domerror.New(FailedOpenChannelKey, "failed to open channel", domerror.Error)
	UnableDeclareExchangeError  = domerror.New(UnableDeclareExchangeKey, "", domerror.Error)
	UnableDeclareQueueError     = domerror.New(UnableDeclareQueueKey, "", domerror.Error)
	UnableDeleteQueueError      = domerror.New(UnableDeleteQueueKey, "", domerror.Error)
	UnableBindQueueError        = domerror.New(UnableBindQueueKey, "", domerror.Error)
	UnableUnbindQueueError      = domerror.New(UnableUnbindQueueKey, "", domerror.Error)
	UnablePublishMessageError   = domerror.New(UnablePublishMessageKey, "", domerror.Error)
	UnableQoSError              = domerror.New(UnableQoSKey, "", domerror.Error)
	UnableAckMessageError       = domerror.New(UnableAckMessageKey, "unable to acknowlege message", domerror.Error)
	FailedRegisterConsumerError = domerror.New(FailedRegisterConsumerKey, "", domerror.Error)
)

func NewUnableToConnectError(msg string, err error) *domerror.DomainError {
	domErr := domerror.SetMessage(UnableToConnectError, msg)
	domErr.Wrap(err)
	return domErr
}

func NewFailedOpenChannelError(err error) *domerror.DomainError {
	return domerror.Wrap(FailedOpenChannelError, err)
}

func NewUnableDeclareExchangeError(exchangeName string, err error) *domerror.DomainError {
	domErr := domerror.Wrap(UnableDeclareExchangeError, err)
	return domerror.SetMessage(domErr, "unable to declare exchange: "+exchangeName)
}

func NewUnableDeclareQueueError(queueName string, err error) *domerror.DomainError {
	domErr := domerror.Wrap(UnableDeclareQueueError, err)
	return domerror.SetMessage(domErr, "unable to declare queue: "+queueName)
}

func NewUnableDeleteQueueError(queueName string, err error) *domerror.DomainError {
	domErr := domerror.Wrap(UnableDeleteQueueError, err)
	return domerror.SetMessage(domErr, "unable to delete queue: "+queueName)
}

func NewUnableBindQueueError(queueName, exchangeName string, err error) *domerror.DomainError {
	domErr := domerror.Wrap(UnableBindQueueError, err)
	return domerror.SetMessage(domErr, "unable to bind queue: "+queueName+" to exchangeName: "+exchangeName)
}

func NewUnableUnbindQueueError(queueName, exchangeName string, err error) *domerror.DomainError {
	domErr := domerror.Wrap(UnableUnbindQueueError, err)
	return domerror.SetMessage(domErr, "unable to unbind queue: "+queueName+" to exchangeName: "+exchangeName)
}

func NewUnablePublishMessageError(routingKey, exchangeName string, err error) *domerror.DomainError {
	domErr := domerror.Wrap(UnablePublishMessageError, err)
	return domerror.SetMessage(domErr, "unable to publish message in exchange: "+exchangeName+" with routing key: "+routingKey)
}

func NewUnableQoSError(queueName string, err error) *domerror.DomainError {
	domErr := domerror.Wrap(UnableQoSError, err)
	return domerror.SetMessage(domErr, "unable QoS to queue: "+queueName)
}

func NewUnableAckMessageError(err error) *domerror.DomainError {
	return domerror.Wrap(UnableAckMessageError, err)
}

func NewFailedRegisterConsumerError(queueName string, err error) *domerror.DomainError {
	domErr := domerror.Wrap(FailedRegisterConsumerError, err)
	return domerror.SetMessage(domErr, "failed to register a consumer on queue: "+queueName)
}
