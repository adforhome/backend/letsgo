package rabbitmq

import (
	"github.com/google/uuid"
	"github.com/streadway/amqp"

	"gitlab.com/adforhome/backend/letsgo/domerror"
)

func (c *Client) getReusableReceiveChannel() (*amqp.Channel, *domerror.DomainError) {
	c.Lock()
	if c.receiveChannel == nil {
		c.Unlock()
		var err *domerror.DomainError
		if c.receiveChannel, err = c.OpenChannel(); err != nil {
			return nil, err
		}
	} else {
		c.Unlock()
	}

	return c.receiveChannel, nil
}

// ConsumeQueue consumes rabbitmq queue with one prefetch count
func (c *Client) ConsumeQueue(queue string) (<-chan amqp.Delivery, *domerror.DomainError) {
	return c.ConsumeQueueWithPrefetchCount(queue, 1)
}

// ConsumeQueueWithPrefetchCount consumes rabbitmq queue with a specified prefetch count
func (c *Client) ConsumeQueueWithPrefetchCount(queue string, count int) (<-chan amqp.Delivery, *domerror.DomainError) {
	commandCh, domErr := c.getReusableCommandChannel()
	if domErr != nil {
		return nil, domErr
	}

	receiveCh, domErr := c.getReusableReceiveChannel()
	if domErr != nil {
		return nil, domErr
	}

	c.Lock()
	err := commandCh.Qos(
		count, // prefetch count
		0,     // prefetch size
		false, // global
	)
	c.Unlock()
	if err != nil {
		return nil, NewUnableQoSError(queue, err)
	}

	consumerTag := uuid.Must(uuid.NewRandom()).String()
	c.Lock()
	msgs, err := receiveCh.Consume(
		queue,       // queue
		consumerTag, // consumer
		false,       // auto-ack
		false,       // exclusive
		false,       // no-local
		false,       // no-wait
		nil,         // args
	)
	c.Unlock()
	if err != nil {
		return nil, NewFailedRegisterConsumerError(queue, err)
	}

	log.Infof("consuming queue '%s'", queue)
	return msgs, nil
}
