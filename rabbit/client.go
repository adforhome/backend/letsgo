package rabbitmq

import (
	"sync"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/adforhome/backend/letsgo/config"
	"gitlab.com/adforhome/backend/letsgo/domerror"
)

var log = logrus.WithField("setting", "rabbit")

type Client struct {
	connection *amqp.Connection

	commandChannel *amqp.Channel
	receiveChannel *amqp.Channel
	sendChannel    *amqp.Channel

	mutex sync.Mutex
}

// NewClient will retrieve the supposedly present RabbitMQ configuration and
// return a new *rabbitmq.Client.
func NewClient() (*Client, *domerror.DomainError) {
	uri, domErr := config.MustConfig("rabbit.uri")
	if domErr != nil {
		return nil, domErr
	}

	conn, err := amqp.Dial(uri)
	if err != nil {
		domErr := NewUnableToConnectError("unable to connect rabbit", err)

		return nil, domErr
	}

	log.Infoln("successfully connected to RabbitMQ: ", uri)
	return &Client{connection: conn}, nil
}

func (c *Client) Close() error {
	return c.connection.Close()
}

func (c *Client) Lock() {
	c.mutex.Lock()
}

func (c *Client) Unlock() {
	c.mutex.Unlock()
}
