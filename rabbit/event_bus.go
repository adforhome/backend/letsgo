package rabbitmq

import (
	"context"
	"encoding/json"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/adforhome/backend/letsgo/domerror"
	"gitlab.com/adforhome/backend/letsgo/logger"
)

type EventRegister interface {
	// EventFactory will return new struct of its own type
	EventFactory() EventHandler
	// GetQueueName returns the name of the queue to listen
	GetQueueName() string
	// GetRoutingKey returns the name of the event to handle (routing key)
	GetRoutingKey() string
	// GetExchangeName returns the name of the exchange
	GetExchangeName() string
}

type EventHandler interface {
	// Handle will process the event
	Handle(ctx context.Context, ack Acknowledger) *domerror.DomainError
}

type Event struct {
	QueueName, RoutingKey, ExchangeName string
}

func (e Event) GetQueueName() string {
	return e.QueueName
}

func (e Event) GetRoutingKey() string {
	return e.RoutingKey
}

func (e Event) GetExchangeName() string {
	return e.ExchangeName
}

type Acknowledger struct {
	amqp.Acknowledger
	deliveryTag uint64
}

func (ack Acknowledger) Ack(multiple bool) *domerror.DomainError {
	if err := ack.Acknowledger.Ack(ack.deliveryTag, multiple); err != nil {
		return NewUnableAckMessageError(err)
	}
	return nil
}

func (ack Acknowledger) Nack(multiple, requeue bool) *domerror.DomainError {
	if err := ack.Acknowledger.Nack(ack.deliveryTag, multiple, requeue); err != nil {
		return NewUnableAckMessageError(err)
	}
	return nil
}

func (ack Acknowledger) Reject(requeue bool) *domerror.DomainError {
	if err := ack.Acknowledger.Reject(ack.deliveryTag, requeue); err != nil {
		return NewUnableAckMessageError(err)
	}
	return nil
}

// ListenForEvents will start listening for all registered
// events
func ListenForEvents(client *Client, events ...EventRegister) *domerror.DomainError {
	for _, q := range events {
		rabbitMsg := q.(EventRegister)
		if err := client.DeclarePersistentQueue(rabbitMsg.GetQueueName()); err != nil {
			return err
		}
		if err := client.BindQueueToExchange(rabbitMsg.GetQueueName(), rabbitMsg.GetExchangeName(), q.GetRoutingKey()); err != nil {
			return err
		}

		messages, err := client.ConsumeQueueWithPrefetchCount(rabbitMsg.GetQueueName(), 3)
		if err != nil {
			return err
		}

		go consumeQueue(q, messages)
	}

	return nil
}

func consumeQueue(queue EventRegister, messages <-chan amqp.Delivery) {
	for d := range messages {
		go handleMessage(queue, d)
	}
}

func handleMessage(queue EventRegister, d amqp.Delivery) {
	msg := queue.EventFactory()

	ctx := ContextFromMessage(d)
	logrus.Infof("--> EVENT: %s: %s", queue.GetExchangeName(), queue.GetRoutingKey())

	if err := json.Unmarshal(d.Body, &msg); err != nil {
		logger.LogDomainError(nil, domerror.NewFailedUnmarshalError(msg, err))
		_ = d.Ack(false)
	} else {
		if err := msg.Handle(ctx, Acknowledger{d.Acknowledger, d.DeliveryTag}); err != nil {
			logger.WithDomainError(nil, err)
			_ = d.Ack(true)
		}
	}
}
