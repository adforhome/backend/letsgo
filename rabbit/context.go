package rabbitmq

import (
	"context"
	"time"

	"github.com/streadway/amqp"
)

func ContextFromMessage(d amqp.Delivery) context.Context {
	ctx := context.Background()

	ctx = setContextFromHeader(ctx, "commandID", &d.Headers)
	ctx = setContextFromHeader(ctx, "commandName", &d.Headers)
	ctx = context.WithValue(ctx, "startTime", time.Now())

	return ctx
}

func setContextFromHeader(ctx context.Context, key string, headers *amqp.Table) context.Context {
	if value, ok := (*headers)[key]; ok {
		return context.WithValue(ctx, key, value)
	}
	return ctx
}
