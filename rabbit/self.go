package rabbitmq

import (
	"fmt"
	"sync"

	"github.com/google/uuid"
	"github.com/streadway/amqp"

	"gitlab.com/adforhome/backend/letsgo/config"
	"gitlab.com/adforhome/backend/letsgo/domerror"
)

var selfMutex = &sync.Mutex{}
var selfQueueName string

func GetSelfQueueName() (string, *domerror.DomainError) {
	selfMutex.Lock()
	defer selfMutex.Unlock()

	if selfQueueName == "" {
		var serviceName string
		serviceName, err := config.MustConfig("service.name")
		if err != nil {
			return "", err
		}
		selfQueueName = fmt.Sprintf("%s.self.%s", serviceName, uuid.New().String())
	}

	return selfQueueName, nil
}

// DeclareSelfQueue will prepare and register a self queue for this service. You should call
// ConsumeSelfQueue() after that.
func (c *Client) DeclareSelfQueue() *domerror.DomainError {
	// declare self queue
	selfQueueOptions := map[string]bool{
		"durable":    false,
		"autodelete": true,
		"exclusive":  true,
		"nowait":     false,
	}
	selfQueueName, err := GetSelfQueueName()
	if err != nil {
		return err
	}
	if err := c.DeclareQueue(selfQueueName, selfQueueOptions); err != nil {
		return err
	}

	return nil
}

// DeleteSelfQueue will delete the self queue for this service.
func (c *Client) DeleteSelfQueue() *domerror.DomainError {
	// declare self queue
	selfQueueOptions := map[string]bool{
		"ifunused": false,
		"ifempty":  false,
		"nowait":   false,
	}
	selfQueueName, err := GetSelfQueueName()
	if err != nil {
		return err
	}
	if err := c.DeleteQueue(selfQueueName, selfQueueOptions); err != nil {
		return err
	}

	return nil
}

func (c *Client) ConsumeSelfQueue() (<-chan amqp.Delivery, *domerror.DomainError) {
	selfQueueName, domErr := GetSelfQueueName()
	if domErr != nil {
		return nil, domErr
	}
	channel, err := c.ConsumeQueue(selfQueueName)
	if err != nil {
		return nil, err
	}
	return channel, nil
}

func (c *Client) ConsumeSelfQueueWithPrefetchCount(count int) (<-chan amqp.Delivery, *domerror.DomainError) {
	selfQueueName, domErr := GetSelfQueueName()
	if domErr != nil {
		return nil, domErr
	}
	channel, err := c.ConsumeQueueWithPrefetchCount(selfQueueName, count)
	if err != nil {
		return nil, err
	}
	return channel, nil
}
