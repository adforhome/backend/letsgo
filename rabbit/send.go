package rabbitmq

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"time"

	"github.com/streadway/amqp"

	"gitlab.com/adforhome/backend/letsgo/domerror"
)

func (c *Client) getReusableSendChannel() (*amqp.Channel, *domerror.DomainError) {
	c.Lock()
	if c.sendChannel == nil {
		c.Unlock()
		var err *domerror.DomainError
		if c.sendChannel, err = c.OpenChannel(); err != nil {
			return nil, err
		}
	} else {
		c.Unlock()
	}

	return c.sendChannel, nil
}

// GetReusableSendChannelAtYourOwnRisk should be temporary for the migration of librariesgo to v2
func (c *Client) GetReusableSendChannelAtYourOwnRisk() (*amqp.Channel, error) {
	return c.getReusableSendChannel()
}

// SendMessageToQueue allows to send a messsage directly to a queue.
func (c *Client) SendMessageToQueue(ctx context.Context, queue string, msgPayload json.RawMessage) error {
	var err error
	var ch *amqp.Channel

	if ch, err = c.getReusableSendChannel(); err != nil {
		return fmt.Errorf("failed to retrieve the reusable send channel: %w", err)
	}

	return sendMessage(ctx, ch, "", queue, amqp.Publishing{
		ContentType: "application/json",
		Body:        msgPayload,
	})
}

// SendMessageToExchange allows to send a messsage to an exchange. The routing key won't be used if
// it's a fanout exchange.
func (c *Client) SendMessageToExchange(ctx context.Context, exchange, routingKey string, msgPayload json.RawMessage, header amqp.Table) *domerror.DomainError {
	var err *domerror.DomainError
	var ch *amqp.Channel

	if ch, err = c.getReusableSendChannel(); err != nil {
		return err
	}

	return sendMessage(ctx, ch, exchange, routingKey, amqp.Publishing{
		ContentType: "application/json",
		Body:        msgPayload,
		Headers:     header,
	})
}

func sendMessage(ctx context.Context, ch *amqp.Channel, exchange, routingKey string, msg amqp.Publishing) *domerror.DomainError {
	// avoid trying to write in a nil map
	if msg.Headers == nil {
		msg.Headers = make(amqp.Table)
	}
	if ctx.Value("commandID") != "" {
		msg.Headers["commandID"] = ctx.Value("commandID")
	}

	startTime := ctx.Value("startTime").(time.Time)
	duration := time.Since(startTime)

	logrus.Infof("<-- EVENT: %s: %s %dms", exchange, routingKey, duration.Milliseconds())

	err := ch.Publish(
		exchange,
		routingKey,
		false,
		false,
		msg,
	)
	if err != nil {
		return NewUnablePublishMessageError(routingKey, exchange, err)
	}

	return nil
}
