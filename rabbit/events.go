package rabbitmq

import (
	"context"
	"encoding/json"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/streadway/amqp"

	"gitlab.com/adforhome/backend/letsgo/config"
	"gitlab.com/adforhome/backend/letsgo/domerror"
	"gitlab.com/adforhome/backend/letsgo/handle_retry"
)

const EventExchangeName = "services.events"

var amqpMessageProcessed = promauto.NewCounterVec(
	prometheus.CounterOpts{
		Namespace: "tadaweb",
		Name:      "amqp_message_processed_total",
		Help:      "The total number of AMQP message processed",
	},
	[]string{"status", "routing_key"})

var amqpMessageDuration = promauto.NewHistogramVec(
	prometheus.HistogramOpts{
		Namespace: "tadaweb",
		Name:      "amqp_message_duration_seconds",
		Buckets:   []float64{0.3, 0.6, 1, 2, 5, 10, 20, 60},
		Help:      "The duration of each message processed in seconds",
	},
	[]string{"routing_key"})

var eventQueueName string

type EventSender interface {
	Send(ctx context.Context) *domerror.DomainError
}

type EventEmitter struct {
	Exchange   string
	RoutingKey string
	Header     amqp.Table
	Body       interface{}
}

type EventHandleFunc func(amqp.Delivery) *domerror.DomainError

type EventSubscription struct {
	Queue      string
	Exchange   string
	RoutingKey string
	Handle     EventHandleFunc
	//bool for activating/deactivating concurrent process?
	//bool for killing service or not if failed to process the event?
}

// EventForwardToAllInstanceHandleFunc is a EventHandleFunc which is use when you want all your instances
// being aware of this event. It will take the event and forward it to the service all exchange. All instances
// of the service are supposed to bind their self queues to this fanout exchange.
func (c *Client) EventForwardToAllInstanceHandleFunc(ctx context.Context, d amqp.Delivery, exchangeName string) *domerror.DomainError {
	var domErr *domerror.DomainError
	var ch *amqp.Channel

	if ch, domErr = c.getReusableSendChannel(); domErr != nil {
		return domErr
	}

	// only copying application values
	var msg amqp.Publishing
	msg.Headers = d.Headers
	msg.ContentType = d.ContentType
	msg.ContentEncoding = d.ContentEncoding
	msg.CorrelationId = d.CorrelationId
	msg.ReplyTo = d.ReplyTo
	msg.MessageId = d.MessageId
	msg.Type = d.Type
	msg.UserId = d.UserId
	msg.AppId = d.AppId
	msg.Body = d.Body

	if err := sendMessage(ctx, ch, exchangeName, d.RoutingKey, msg); err != nil {
		log.WithError(err).Warningln("failed to forward 'asset.ready' to all instances")
		return err
	}

	return nil
}

func (c *Client) ListenEvents(ctx context.Context, subscriptions []EventSubscription, stop chan<- bool) *domerror.DomainError {
	var err *domerror.DomainError

	eventQueueName, err = config.MustConfig("service.name")
	if err != nil {
		return err
	}
	eventQueueName += ".events.all"

ChannelLoop:
	for err == nil {
		// preparing a persistant queue for forwarding events to
		if err = c.DeclarePersistentQueue(eventQueueName); err != nil {
			continue
		}

		// binding to the requested events
		for _, s := range subscriptions {
			//REMARK you can't bind twice with the same arguments, no risk of multiple bindings
			if err = c.BindQueueToExchange(eventQueueName, EventExchangeName, s.RoutingKey); err != nil {
				continue
			}
		}

		// start consuming events from the shared queue
		events := make(<-chan amqp.Delivery)
		events, err = c.ConsumeQueue(eventQueueName)
		if err != nil {
			continue
		}

		for e := range events {
			log.Debugln("consuming one event from:", e.RoutingKey)

			// check if the message hasn't been redelivered (because it may caused a crash before or failed to process it)
			// if so, we'll ack it immediately for avoiding crashing/failling in loop
			if e.Redelivered {
				log.Warningln("event already delivered once, acking it in advance")
				e.Ack(false)
			}

			subscriptionFound := false
			for _, s := range subscriptions {
				if s.RoutingKey == e.RoutingKey {
					subscriptionFound = true
					now := time.Now()
					if err = s.Handle(e); err != nil {
						if !e.Redelivered {
							e.Reject(true)
							log.WithError(err).Warningln("failed to handle one event, requeuing it")
						} else {
							log.WithError(err).Errorln("failed to handle one event, already tried twice, giving up")
						}

						amqpMessageProcessed.With(prometheus.Labels{"status": "ERROR", "routing_key": e.RoutingKey}).Inc()
						amqpMessageDuration.With(prometheus.Labels{"routing_key": e.RoutingKey}).Observe(time.Since(now).Seconds())

						//TODO should we really give up completely?
						break ChannelLoop
					} else {
						// ack the event if it was not already acked because it was a redelivery
						if !e.Redelivered {
							e.Ack(false)
						}

						amqpMessageProcessed.With(prometheus.Labels{"status": "OK", "routing_key": e.RoutingKey}).Inc()
						amqpMessageDuration.With(prometheus.Labels{"routing_key": e.RoutingKey}).Observe(time.Since(now).Seconds())
					}
					// stop looking further in the subscription list, we found it
					break
				}
			}

			if !subscriptionFound {
				// not supposed to happen, except if they are some unexpected/undeleted bindings
				e.Reject(false)
			}
		}
	}

	stop <- true
	return nil
}

// EmitHeaderBodyEvent used for compatibility with V1 rabbitmq.Emit()
func (c *Client) EmitHeaderBodyEvent(ctx context.Context, event EventEmitter, options ...handle_retry.SetHandleRetryOptions) *domerror.DomainError {
	data, err := json.Marshal(event.Body)
	if err != nil {
		return domerror.NewUnableMarshalError(event.Body, err)
	}

	return handle_retry.HandleRetry(func() *domerror.DomainError {
		return c.SendMessageToExchange(ctx, event.Exchange, event.RoutingKey, data, event.Header)
	}, options...)
}
