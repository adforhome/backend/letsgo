package rabbitmq

import (
	"github.com/streadway/amqp"

	"gitlab.com/adforhome/backend/letsgo/domerror"
)

const (
	// ExchangeTypeDirect delivers messages to queues based on the message routing key
	ExchangeTypeDirect ExchangeType = "direct"
	// ExchangeTypeFanout routes messages to all of the queues that are bound to it and the routing key is ignored
	ExchangeTypeFanout ExchangeType = "fanout"
	// ExchangeTypeTopic routes messages to one or many queues based on matching between a message routing key and the pattern that was used to bind a queue to an exchange
	ExchangeTypeTopic ExchangeType = "topic"
	// ExchangeTypeHeaders routes on multiple attributes that are more easily expressed as message headers than a routing key
	ExchangeTypeHeaders ExchangeType = "match"
)

// ExchangeType the type of the exchange
type ExchangeType string

func (c *Client) getReusableCommandChannel() (*amqp.Channel, *domerror.DomainError) {
	c.Lock()
	if c.commandChannel == nil {
		c.Unlock()
		var err *domerror.DomainError
		if c.commandChannel, err = c.OpenChannel(); err != nil {
			return nil, err
		}
	} else {
		c.Unlock()
	}

	return c.commandChannel, nil
}

// OpenChannel will prepare and return a channel. You're responsible of the life of the channel.
// It will stay open until you close it or the connection is finished.
func (c *Client) OpenChannel() (*amqp.Channel, *domerror.DomainError) {
	c.Lock()
	chann, err := c.connection.Channel()
	c.Unlock()
	if err != nil {
		return nil, NewFailedOpenChannelError(err)
	}

	log.Debug("opened a channel")
	return chann, nil
}

// DeclareExchange create an exchange with the given name and type
func (c *Client) DeclareExchange(name string, eType ExchangeType, options map[string]bool) *domerror.DomainError {
	var durable, autoDelete, internal, noWait bool

	if v, ok := options["durable"]; ok {
		durable = v
	}

	if v, ok := options["autodelete"]; ok {
		autoDelete = v
	}

	if v, ok := options["internal"]; ok {
		internal = v
	}

	if v, ok := options["nowait"]; ok {
		noWait = v
	}

	ch, domErr := c.getReusableCommandChannel()
	if domErr != nil {
		return domErr
	}

	c.Lock()
	err := ch.ExchangeDeclare(
		name,          // exchange name
		string(eType), // exchange type
		durable,       // durable
		autoDelete,    // auto delete
		internal,      // internal
		noWait,        // no-wait
		amqp.Table{},  // args
	)
	c.Unlock()
	if err != nil {
		return NewUnableDeclareExchangeError(name, err)
	}

	log.Debugf("declared exchange '%s'", name)
	return nil
}

func (c *Client) DeclarePersistentQueue(name string) *domerror.DomainError {
	options := map[string]bool{
		"durable":    true,
		"autodelete": false,
		"exclusive":  false,
		"nowait":     false,
	}

	return c.DeclareQueue(name, options)
}

// DeclareQueue create a queue with the given name and options
func (c *Client) DeclareQueue(name string, options map[string]bool) *domerror.DomainError {
	var durable, autoDelete, exclusive, noWait bool

	if v, ok := options["durable"]; ok {
		durable = v
	}

	if v, ok := options["autodelete"]; ok {
		autoDelete = v
	}

	if v, ok := options["exclusive"]; ok {
		exclusive = v
	}

	if v, ok := options["nowait"]; ok {
		noWait = v
	}

	ch, domErr := c.getReusableCommandChannel()
	if domErr != nil {
		return domErr
	}

	c.Lock()
	_, err := ch.QueueDeclare(
		name,         // queue name
		durable,      // durable
		autoDelete,   // auto delete
		exclusive,    // exclusive
		noWait,       // no-wait
		amqp.Table{}, // args
	)
	c.Unlock()
	if err != nil {
		return NewUnableDeclareQueueError(name, err)
	}

	log.Debugf("declared queue '%s'", name)
	return nil
}

// DeleteQueue deletes the queue with the given name
func (c *Client) DeleteQueue(name string, options map[string]bool) *domerror.DomainError {
	var ifUnused, ifEmpty, noWait bool

	if v, ok := options["ifunused"]; ok {
		ifUnused = v
	}

	if v, ok := options["ifempty"]; ok {
		ifEmpty = v
	}

	if v, ok := options["nowait"]; ok {
		noWait = v
	}

	ch, domErr := c.getReusableCommandChannel()
	if domErr != nil {
		return domErr
	}

	c.Lock()
	_, err := ch.QueueDelete(
		name,     // queue name
		ifUnused, // only delete if unused
		ifEmpty,  // only delete if empty
		noWait,   // no-wait
	)
	c.Unlock()
	if err != nil {
		return NewUnableDeleteQueueError(name, err)
	}

	log.Debugf("deleted queue '%s'", name)
	return nil
}

func (c *Client) BindQueueToExchange(queue, exchange, routingKey string) *domerror.DomainError {
	ch, domErr := c.getReusableCommandChannel()
	if domErr != nil {
		return domErr
	}

	c.Lock()
	err := ch.QueueBind(
		queue,      // queue name
		routingKey, // routing key
		exchange,   // exchange
		false,
		nil,
	)
	c.Unlock()
	if err != nil {
		return NewUnableBindQueueError(queue, exchange, err)
	}

	log.Debugf("bound queue '%s' on exchange '%s' with routing key '%s'", queue, exchange, routingKey)
	return nil
}

func (c *Client) UnbindQueueFromExchange(queue, exchange, routingKey string) *domerror.DomainError {
	ch, domErr := c.getReusableCommandChannel()
	if domErr != nil {
		return domErr
	}

	c.Lock()
	err := ch.QueueUnbind(
		queue,        // queue name
		routingKey,   // routing key
		exchange,     // exchange
		amqp.Table{}, // empty?
	)
	c.Unlock()
	if err != nil {
		return NewUnableUnbindQueueError(queue, exchange, err)
	}

	log.Debugf("unbound queue '%s' from exchange '%s' with routing key '%s'", queue, exchange, routingKey)
	return nil
}

func (c *Client) BindExchangeToExchange(src, routingKey, dst string) *domerror.DomainError {
	ch, domErr := c.getReusableCommandChannel()
	if domErr != nil {
		return domErr
	}

	c.Lock()
	err := ch.ExchangeBind(
		dst,
		routingKey,
		src,
		false,
		nil,
	)
	c.Unlock()
	if err != nil {
		return NewUnableBindQueueError("(exchange) "+dst, src, err)
	}

	log.Debugf("bound exchange '%s' on exchange '%s' with routing key '%s'", src, dst, routingKey)
	return nil
}

func (c *Client) UnbindExchangeFromExchange(src, routingKey, dst string) error {
	ch, domErr := c.getReusableCommandChannel()
	if domErr != nil {
		return domErr
	}

	c.Lock()
	err := ch.ExchangeUnbind(
		dst,
		routingKey,
		src,
		true,
		nil,
	)
	c.Unlock()
	if err != nil {
		return NewUnableUnbindQueueError("(exchange) "+src, dst, err)
	}

	log.Debugf("unbound exchange '%s' from exchange '%s' with routing key '%s'", src, dst, routingKey)
	return nil
}
