package mongo

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/adforhome/backend/letsgo/domerror"
)

func GetMongoClient(ctx context.Context, uri string) (*mongo.Client, func(), *domerror.DomainError) {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		return nil, nil, domerror.Wrap(NewUnableToConnectError("unable to connect with uri"), err)
	}
	logrus.WithField("setting", "mongo").Infof("successfully connected to Mongodb: %s", uri)

	disconnect := func() {
		if err = client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}
	return client, disconnect, nil
}
