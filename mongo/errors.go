package mongo

import "gitlab.com/adforhome/backend/letsgo/domerror"

const (
	UnableToConnectKey  domerror.Key = "mongo_unable_to_connect"
	FailedMongoQueryKey domerror.Key = "mongo_failed_query"
)

var (
	UnableToConnectError  = domerror.New(UnableToConnectKey, "unable to connect mongo", domerror.Error)
	FailedMongoQueryError = domerror.New(FailedMongoQueryKey, "", domerror.Error)
)

func NewUnableToConnectError(msg string) *domerror.DomainError {
	return domerror.SetMessage(UnableToConnectError, msg)
}

func NewFailedMongoQueryError(action string, err error) *domerror.DomainError {
	domErr := domerror.Wrap(FailedMongoQueryError, err)
	return domerror.SetMessage(domErr, "query failed: "+action)
}
