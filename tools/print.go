package tools

import (
	"encoding/json"
	"fmt"
)

func PrintStruct(st interface{}) {
	b, _ := json.MarshalIndent(st, "", "  ")
	fmt.Println(string(b))
}
