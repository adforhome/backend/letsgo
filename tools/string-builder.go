package tools

import (
	"fmt"
	"strings"
)

type StringBuilder struct {
	strings.Builder
}

func (s *StringBuilder) WriteLn() *StringBuilder {
	s.WriteString("\n")
	return s
}

func (s *StringBuilder) WriteLt(deep int) *StringBuilder {
	for i := 0; i < deep; i++ {
		s.WriteString("    ")
	}
	return s
}

func (s *StringBuilder) WriteStringf(str string, a ...interface{}) *StringBuilder {
	s.WriteString(fmt.Sprintf(str, a...))
	return s
}

func (s *StringBuilder) WriteStr(str string) *StringBuilder {
	s.WriteString(str)
	return s
}
