package hasura

import "gitlab.com/adforhome/backend/letsgo/domerror"

const (
	UnableToConnectKey   domerror.Key = "hasura_unable_to_connect"
	ErrorFromResponseKey domerror.Key = "hasura_error_from_response"
)

var (
	UnableToConnectError   = domerror.New(UnableToConnectKey, "unable to connect to hasura", domerror.Error)
	ErrorFromResponseError = domerror.New(ErrorFromResponseKey, "error from http response", domerror.Error)
)

func NewUnableToConnectHasuraError() *domerror.DomainError {
	return UnableToConnectError
}

func NewErrorFromResponseError(url, status, body string) *domerror.DomainError {
	return domerror.SetMessage(ErrorFromResponseError, "error from "+url+" status: "+status+" body: "+body)
}
