package hasura_test

import (
	"testing"

	"gitlab.com/adforhome/backend/letsgo/hasura"
)

func TestNewScalars(t *testing.T) {
	if got := hasura.NewBoolean(false); got == nil {
		t.Error("NewBoolean returned nil")
	}
	if got := hasura.NewFloat(0.0); got == nil {
		t.Error("NewFloat returned nil")
	}
	// ID with underlying type string.
	if got := hasura.NewID(""); got == nil {
		t.Error("NewID returned nil")
	}
	// ID with underlying type int.
	if got := hasura.NewID(0); got == nil {
		t.Error("NewID returned nil")
	}
	if got := hasura.NewInt(0); got == nil {
		t.Error("NewInt returned nil")
	}
	if got := hasura.NewString(""); got == nil {
		t.Error("NewString returned nil")
	}
}
