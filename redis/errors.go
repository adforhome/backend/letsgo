package redis

import (
	"fmt"

	"gitlab.com/adforhome/backend/letsgo/domerror"
)

const (
	WaitSetTimeoutKey    domerror.Key = "redis_timed_out_set_expirable_key"
	SetKeyNXKey          domerror.Key = "redis_set_expirable_key"
	NotFoundKey          domerror.Key = "redis_key_not_found"
	ConnectionKey        domerror.Key = "redis_connection_error"
	SetValueKey          domerror.Key = "redis_set_value_error"
	InvalidValueKey      domerror.Key = "redis_invalid_value"
	UnableDeleteValueKey domerror.Key = "redis_unable_delete_value"
)

var (
	WaitSetTimeoutError    = domerror.New(WaitSetTimeoutKey, "WaitToSet timed out!", domerror.Warning)
	SetKeyNXError          = domerror.New(SetKeyNXKey, "failed to create the key (already existing?)", domerror.Warning)
	NotFoundError          = domerror.New(NotFoundKey, "unable to get the value from redis", domerror.Info)
	ConnectionError        = domerror.New(ConnectionKey, "failed to connect to redis", domerror.Warning)
	SetValueError          = domerror.New(SetValueKey, "cannot set redis value", domerror.Warning)
	InvalidValueError      = domerror.New(InvalidValueKey, "received an invalid value from redis", domerror.Warning)
	UnableDeleteValueError = domerror.New(UnableDeleteValueKey, "", domerror.Error)
)

func NewWaitSetTimeoutError() *domerror.DomainError {
	return WaitSetTimeoutError
}

func NewSetKeyNXError() *domerror.DomainError {
	return SetKeyNXError
}

func NewRedisNotFoundError(err error) *domerror.DomainError {
	return domerror.Wrap(NotFoundError, err)
}

func NewRedisConnectionError(err error) *domerror.DomainError {
	return domerror.Wrap(ConnectionError, err)
}

func NewRedisSetValueError(err error) *domerror.DomainError {
	return domerror.Wrap(SetValueError, err)
}

func NewRedisInvalidValueError(err error) *domerror.DomainError {
	return domerror.Wrap(InvalidValueError, err)
}

func NewUnableDeleteValueError(err error, keys ...string) *domerror.DomainError {
	domErr := domerror.SetMessage(UnableDeleteValueError, fmt.Sprintf("unable to delete key: %s", keys))
	return domerror.Wrap(domErr, err)
}
