package redis

import (
	"encoding/json"
	"net"
	"time"

	lock "github.com/bsm/redis-lock"
	"github.com/go-redis/redis"
	log "github.com/sirupsen/logrus"

	"gitlab.com/adforhome/backend/letsgo/config"
	"gitlab.com/adforhome/backend/letsgo/domerror"
)

type Client struct {
	client          *redis.Client
	applicationLock *lock.Locker
}

func isTimeoutError(e error) bool {
	if opError, ok := e.(*net.OpError); ok {
		return opError.Timeout()
	}
	return false
}

// NewClient prepare a Client using the given configuration
func NewClient() (Client, *domerror.DomainError) {
	var rc Client

	db, err := config.MustConfigInt("redis.database")
	if err != nil {
		return Client{}, err
	}

	host, err := config.MustConfig("redis.host")
	if err != nil {
		return Client{}, err
	}

	port, err := config.MustConfig("redis.port")
	if err != nil {
		return Client{}, err
	}

	password, err := config.MustConfig("redis.password")
	if err != nil {
		return Client{}, err
	}

	opt := &redis.Options{
		Addr:     host + ":" + port,
		Password: password,
		DB:       db,
	}

	rc.client = redis.NewClient(opt)

	if err := rc.CheckConnection(); err != nil {
		return rc, err
	}

	log.Infof("successfully connected to Redis: redis://%s@%s:%s/%d", password, host, port, db)

	return rc, nil
}

func (rc *Client) CheckConnection() *domerror.DomainError {
	_, err := rc.client.Ping().Result()
	if err != nil {
		return NewRedisConnectionError(err)
	}
	return nil
}

func (rc *Client) SetValue(key, value string, expiration time.Duration) *domerror.DomainError {
	err := rc.client.Set(key, value, expiration).Err()
	if err != nil {
		return NewRedisSetValueError(err)
	}
	return nil
}

func (rc *Client) GetValueString(key string) (string, *domerror.DomainError) {
	v, e := rc.client.Get(key).Result()
	if isTimeoutError(e) {
		v, e = rc.client.Get(key).Result()
	}
	if e != nil {
		return "", NewRedisNotFoundError(e)
	}

	return v, nil
}

//GetValue get the redis and try to push it into data, data need to be a pointer
func (rc *Client) GetValue(key string, data interface{}) *domerror.DomainError {
	v, err := rc.GetValueString(key)
	if err != nil {
		return err
	}

	if err := json.Unmarshal([]byte(v), data); err != nil {
		return domerror.NewFailedUnmarshalError(data, err)
	}
	return nil
}

func (rc *Client) GetIO(key string) ([]byte, *domerror.DomainError) {
	input := "io:" + key
	v, e := rc.GetValueString(input)
	if e != nil {
		return []byte{}, e
	}

	return []byte(v), nil
}

func (rc *Client) SetExpirableKeyIfNotExist(key, value string, expiration time.Duration) *domerror.DomainError {
	if rc.client.SetNX(key, value, expiration).Val() == false {
		return NewSetKeyNXError()
	}

	return nil
}

func (rc *Client) WaitToSetExpirableKeyIfNotExist(key, value string, expiration time.Duration) *domerror.DomainError {
	const stepWait time.Duration = 100 * time.Millisecond
	const maxWait time.Duration = 2 * time.Minute

	var currentWait time.Duration
	for {
		if rc.client.SetNX(key, value, expiration).Val() == true {
			return nil
		}

		if currentWait < maxWait {
			currentWait += stepWait
			time.Sleep(stepWait)
		} else {
			break
		}
	}
	return NewWaitSetTimeoutError()
}

func (rc Client) BlockFor(key string, duration time.Duration) *domerror.DomainError {
	return rc.WaitToSetExpirableKeyIfNotExist(key, "", duration)
}

func (rc *Client) DeleteKeys(keys ...string) *domerror.DomainError {
	err := rc.client.Del(keys...).Err()
	if err != nil {
		return NewUnableDeleteValueError(err, keys...)
	}
	return nil
}
