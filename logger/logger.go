package logger

import (
	"github.com/sirupsen/logrus"

	"gitlab.com/adforhome/backend/letsgo/domerror"
)

func WithDomainError(logger *logrus.Entry, err *domerror.DomainError) *logrus.Entry {
	if logger == nil {
		logger = logrus.NewEntry(logrus.StandardLogger())
	}
	return logger.WithField("error", err.ToJSON())
}

func LogDomainError(logger *logrus.Entry, err *domerror.DomainError) {
	logger = WithDomainError(logger, err)
	switch err.Severity {
	case domerror.Error:
		logger.Error(err.Message)
	case domerror.Warning:
		logger.Warning(err.Message)
	case domerror.Debug:
		logger.Debug(err.Message)
	case domerror.Info:
		logger.Info(err.Message)
	default:
		logger.Error("error without severity")
	}
}
