package middleware

import (
	"github.com/gorilla/mux"
)

func UseMiddleware(r *mux.Router, path string) *mux.Router {
	router := r.PathPrefix(path).Subrouter()

	router.Use(ContextHandler)
	router.Use(LoggerHandler)

	return router
}
