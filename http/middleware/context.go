package middleware

import (
	"context"
	"net/http"
)

func ContextHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		commandID := r.Header.Get("x-command-id")

		ctx := context.WithValue(r.Context(), "commandID", commandID)

		h.ServeHTTP(w, r.WithContext(ctx))
	})
}
