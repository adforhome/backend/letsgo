package middleware

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/adforhome/backend/letsgo/http/controller"
)

type statusResponseWriter struct {
	http.ResponseWriter
	Status string
}

func NewStatusResponseWriter(w http.ResponseWriter) *statusResponseWriter {
	return &statusResponseWriter{
		ResponseWriter: w,
	}
}

func (srw *statusResponseWriter) Write(body []byte) (int, error) {
	resp := controller.ResponsePayload{}
	json.Unmarshal(body, &resp)
	srw.Status = string(resp.Status)
	return srw.ResponseWriter.Write(body)
}

func LoggerHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logrus.Infof("--> %s: %s", r.Method, r.URL)

		startRequest := time.Now()
		sw := NewStatusResponseWriter(w)
		r = r.WithContext(context.WithValue(r.Context(), "startTime", startRequest))

		h.ServeHTTP(sw, r)

		durationTime := time.Since(startRequest)

		logrus.Infof("<-- %s: %s %s %dms", r.Method, r.URL, sw.Status, durationTime.Milliseconds())

	})
}
