package http

import (
	"context"
	"net/url"
	"strings"

	"gitlab.com/adforhome/backend/letsgo/config"
	"gitlab.com/adforhome/backend/letsgo/domerror"
)

type VarPath struct {
	K string
	V string
}

func GetURL(service, path string) (url.URL, *domerror.DomainError) {
	urlConfig, domErr := config.MustConfig(service + ".url")
	if domErr != nil {
		return url.URL{}, domErr
	}

	URL, err := url.Parse(urlConfig)
	if err != nil {
		return url.URL{}, NewFailedParseURLError(urlConfig, err)
	}

	URL.Path = path
	return *URL, nil
}

func NewContextFromContext(ctx context.Context) context.Context {
	newCtx := context.Background()
	newCtx = context.WithValue(newCtx, "commandID", ctx.Value("commandID"))
	newCtx = context.WithValue(newCtx, "startTime", ctx.Value("startTime"))
	return newCtx
}

func ReplaceVarPath(path string, vars ...VarPath) string {
	for _, v := range vars {
		path = strings.Replace(path, "{"+v.K+"}", v.V, 1)
	}
	return path
}
