package http

import (
	"encoding/json"
	"net/http"

	"gitlab.com/adforhome/backend/letsgo/domerror"
	"gitlab.com/adforhome/backend/letsgo/http/controller"
)

func DecodeResponse(resp *http.Response, data interface{}) *domerror.DomainError {
	if resp.StatusCode != 200 {
		return NewStatusError(resp.Status, resp.StatusCode)
	}

	payload := &controller.ResponsePayload{}
	json.NewDecoder(resp.Body).Decode(&payload)
	resp.Body.Close()

	switch payload.Status {
	case controller.Fail, controller.Error:
		err := &domerror.DomainError{}
		json.Unmarshal(payload.Data, err)
		return err
	case controller.Success:
		err := json.Unmarshal(payload.Data, data)
		if err != nil {
			return domerror.NewFailedUnmarshalError(data, err)
		}
		return nil
	}
	return NewInvalidPayloadError(string(payload.Status))

}
