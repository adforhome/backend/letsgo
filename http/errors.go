package http

import (
	"strconv"

	"gitlab.com/adforhome/backend/letsgo/domerror"
)

const (
	InvalidResponsePayloadKey domerror.Key = "http_invalid_response_payload"
	ExecutionKey              domerror.Key = "http_execution_error"
	FailedParseURLKey         domerror.Key = "http_failed_parse_url"
	FailedStartServerKey      domerror.Key = "http_failed_start_server"
	StatusKey                 domerror.Key = "http_status_error"
)

var (
	InvalidPayloadStatusError = domerror.New(InvalidResponsePayloadKey, "invalid response payload status", domerror.Error)
	ExecutionError            = domerror.New(ExecutionKey, "execution error", domerror.Error)
	FailedParseURLError       = domerror.New(FailedParseURLKey, "", domerror.Error)
	FailedStartServerError    = domerror.New(FailedStartServerKey, "unable to start http server", domerror.Error)
	StatusError               = domerror.New(StatusKey, "status are not 200", domerror.Error)
)

func NewInvalidPayloadError(status string) *domerror.DomainError {
	return domerror.SetMessage(InvalidPayloadStatusError, "invalid response payload status: "+status)
}

func NewExecutionError(action string, err error) *domerror.DomainError {
	domErr := domerror.SetMessage(ExecutionError, "unable to "+action)
	domErr.Wrap(err)
	return domErr
}

func NewFailedParseURLError(url string, err error) *domerror.DomainError {
	domErr := domerror.SetMessage(FailedParseURLError, "unable to parse url: "+url)
	return domerror.Wrap(domErr, err)
}

func NewFailedStartServerError(err error) *domerror.DomainError {
	return domerror.Wrap(FailedStartServerError, err)
}

func NewStatusError(status string, statusCode int) *domerror.DomainError {
	return domerror.SetMessage(StatusError, "status code are not 200, got code: "+strconv.Itoa(statusCode)+" status '"+status+"'")
}
