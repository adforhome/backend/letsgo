package http

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/adforhome/backend/letsgo/domerror"
)

// NewRequest prepare a new request like http.NewRequest would do. It also fills
// the new request with headers that needs to be propageted to other internal
// services.
// Don't use this when contacting external sevices !
func NewRequest(ctx context.Context, method, url string, body interface{}) (*http.Request, *domerror.DomainError) {
	var req *http.Request
	var err error
	if body != nil {
		bodyJSON, err := json.Marshal(body)
		if err != nil {
			return nil, domerror.NewUnableMarshalError(body, err)
		}
		buff := bytes.NewBuffer(bodyJSON)

		req, err = http.NewRequest(method, url, buff)
		if err != nil {
			return req, NewExecutionError("create new request", err)
		}
	} else {
		req, err = http.NewRequest(method, url, nil)
		if err != nil {
			return req, NewExecutionError("create new request", err)
		}
	}

	if commandID := ctx.Value("commandID"); commandID != nil {
		req.Header.Add("x-command-id", commandID.(string))
	}

	return req, nil
}

// NewRequest prepare a new request like http.NewRequest would do. It also fills
// the new request with headers that needs to be propageted to other internal
// services.
// Don't use this when contacting external sevices !
func NewRequestWithHeader(ctx context.Context, method, url string, body interface{}, headers http.Header) (*http.Request, *domerror.DomainError) {
	req, err := NewRequest(ctx, method, url, body)
	if err != nil {
		return nil, err
	}

	for header, value := range headers {
		for _, v := range value {
			req.Header.Add(header, v)
		}
	}

	return req, nil
}

// Don't use this when contacting external sevices !
func NewClient() *http.Client {
	return &http.Client{
		Timeout: time.Minute * 2,
	}
}

// Don't use this when contacting external sevices !
func Do(ctx context.Context, method, url string, body interface{}) (*http.Response, *domerror.DomainError) {
	return DoWithHeaders(ctx, method, url, body, http.Header{})
}

func DoAndDecodeResponse(ctx context.Context, method, url string, body interface{}, data interface{}) *domerror.DomainError {
	resp, err := Do(ctx, method, url, body)
	if err != nil {
		return err
	}

	return DecodeResponse(resp, data)
}

// Don't use this when contacting external sevices !
func DoWithHeaders(ctx context.Context, method, url string, body interface{}, headers http.Header) (*http.Response, *domerror.DomainError) {
	req, err := NewRequestWithHeader(ctx, method, url, body, headers)
	if err != nil {
		return nil, err
	}

	return do(ctx, req, method)
}

func do(ctx context.Context, req *http.Request, method string) (*http.Response, *domerror.DomainError) {
	client := NewClient()

	if length := req.Header.Get("content-length"); length != "" {
		if size, err := strconv.ParseInt(length, 10, 64); err == nil {
			req.ContentLength = size
		}
	}

	commandID := ctx.Value("commandID")
	if commandID != nil {
		req.Header.Add("x-command-id", commandID.(string))
	}

	if host := req.Header.Get("host"); host != "" {
		req.Host = host
	}
	resp, err := client.Do(req)
	if err != nil {
		return resp, NewExecutionError("do request", err)
	}

	return resp, nil
}
