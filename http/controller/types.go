package controller

import "encoding/json"

type Status string

const (
	Success Status = "success"
	Fail    Status = "fail"
	Error   Status = "error"
)

type ResponsePayload struct {
	Status Status          `json:"status"`
	Data   json.RawMessage `json:"data"`
}

type responsePayload struct {
	Status Status      `json:"status"`
	Data   interface{} `json:"data"`
}
