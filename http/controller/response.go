package controller

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/sirupsen/logrus"

	domerror "gitlab.com/adforhome/backend/letsgo/domerror"
	"gitlab.com/adforhome/backend/letsgo/logger"
)

// SendSuccess sends the data as a json and with status "success" and the status code OK.
func SendSuccess(w http.ResponseWriter, r *http.Request, data interface{}) {
	sendResponse(w, r, http.StatusOK, Success, data)
}

// SendSuccessWithCode sends the data as a json and with status "success" and the status code defined.
//
// *WARNING* should be used with caution, only a few exceptions can justify it
func SendSuccessWithCode(w http.ResponseWriter, r *http.Request, statusCode int, data interface{}) {
	sendResponse(w, r, statusCode, Success, data)
}

// SendError sends the data as a json and with the status "error" and status code defined in the HttpError or status OK if not HttpError.
// Error must be used when the error occures at processing time
func SendError(w http.ResponseWriter, r *http.Request, data error) {
	sendError(w, r, Error, data)
}

// SendFail sends the data as a json and with the status "fail" and status code defined in the HttpError or status OK if not HttpError.
// Fail must be used when the error occures at the preconditions checking
func SendFail(w http.ResponseWriter, r *http.Request, data error) {
	sendError(w, r, Fail, data)
}

func sendError(w http.ResponseWriter, r *http.Request, status Status, data error) {
	var raw domerror.DomainError
	code := http.StatusOK

	errors.As(data, &raw)

	log := logrus.NewEntry(logrus.StandardLogger())

	logger.LogDomainError(log, &raw)

	sendResponse(w, r, code, status, data)
}

func sendResponse(w http.ResponseWriter, r *http.Request, code int, status Status, data interface{}) {
	// Prepare header & status code
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)

	// Prepare body
	json.NewEncoder(w).Encode(responsePayload{
		Status: status,
		Data:   data,
	})
}
