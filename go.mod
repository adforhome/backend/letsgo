module gitlab.com/adforhome/backend/letsgo

require (
	github.com/bsm/redis-lock v8.0.0+incompatible
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/opentracing/opentracing-go v1.2.0 // indirect
	github.com/prometheus/client_golang v1.10.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	github.com/streadway/amqp v1.0.0
	go.mongodb.org/mongo-driver v1.4.5
	golang.org/x/net v0.0.0-20201021035429-f5854403a974
)

go 1.16
